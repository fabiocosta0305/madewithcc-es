# This file

This is just a braindump from Gunnar, from the Spanish translation,
documenting his steps to get from the book to the source translation
material. The following instructions are valid as of June 9, 2017,
19:45 GMT-5, and are mainly written in order not to forget the way
this was done (and in case a future version import comes up, or we
write some scripts to refine the results)

# How is the project built?

How did we get the `.po` files for the project?

## First, an `.odt`

I don't think an Open Document Text (`.odt`) file is the ideal source
format for this project, but according to the conversations I've had
with Sarah, probably it's the best we can do with: It is, after all,
the file they worked with. I would rather start conversion from the
published EPUB file, but the conversion ends up much "noisier".

I downloaded
[the .odt file in the repository](./MadewithCreativeCommonsmostup-to-dateversion.odt)
by following the link listed as
[editable version of the book (GDoc)](https://docs.google.com/document/d/1c2BXTQcWZrtW99GlGmv3waXy3AVpV-Biifw4NdDwMZg/edit#heading=h.3s5bd22z8pw5).
from [the book's web page](https://madewith.cc). I am not that happy
about this being an _unstable_ version (it is after all termed the
_most up-to-date version_).

## Directory structure for the translation

Most translation tools based on
[GNU Gettext](https://www.gnu.org/software/gettext/) use a
standardized directory structure where all translations reside in the
`po` directory. It can feel a bit strange here, as the whole
repository is the translation itself (Gettext is often used to
translate programs, not text), but it's better not to upset common
practice ;-)

Inside the `po` directory, we have one directory per target _locale_
(per target translation). The Spanish translation lives within the
`po/es` directory; in case we were to require country-specific locales
(I don't forsee it being the case), they would be named `po/es_MX`,
`po/es_AR`, etc. This is common in languages where national variations
are stronger, such as Portuguese (would probably be `po/pt_BR`
vs. `po/pt_PT`).

## Quick and easy: How to rebuild the sources

You don't really need to follow this script; it's all scripted in a
`Makefile`. To update the `.pot` (translation template) file, just
run:

    `make pot`

That is enough to get you to the _Editing the Gettext files_ section.

## Markdown conversion

Markdown is an easy to edit format, with very little markup, and
supporting basic HTML tags where needed.

We use Pandoc to convert the `odt` file into Markdown:

    pandoc -f odt MadewithCreativeCommonsmostup-to-dateversion.odt -t markdown > MadewithCreativeCommonsmostup-to-dateversion.md

We also include the source Markdown file, mostly as documentation, in
the Git repository root directory.

### Markdown fixup

The generated Markdown file is correct, but somewhat _dirty_. I came
up with a quick-and-dirty(er) script to fix several issues,
`fixup.rb`, and intend to update it as needed:

    ruby fixup.rb source.md dest.md

I still need to add to this fixup the needed markup so that the built
material reflects the book's structure. It will happen :-) It will, of
course, happen earlier if you send me some patches!

## Extracting strings for translation

Gettext works by presenting each particular string to be edited. We
used the [PO for anything](https://po4a.alioth.debian.org/) (po4a)
tool, particularly the `po4a-gettextize` tool, to analyze the Markdown
file and generate the Gettext template (`.pot`):

    po4a-gettextize -f text -m MadewithCreativeCommonsmostup-to-dateversion.md -p po/mwcc.pot -M utf-8

`.pot` files are the base from which to start a translation, but are
not to be modified, just updated when new versions come up. We copied
this `.pot` file into the Spanish directory, with a `.po` suffix, and
started the translation from there:

    cp po/mwcc.pot po/es/mwcc.po

Should you want to start a new translation, that's the main step to do
to start from. Note that for any future updates, the `.pot` should
_not_ be copied over (as that would destroy all the translation done
so far), but updated using your favorite Gettext tool.

## Editing the Gettext files

There are many tools you can use to translate this file. If you prefer
to work on your client (and mainly if you expect to do a high volume
of changes), we suggest you to use the `poedit` tool; we have also
been accepted as a hosted project
[hosted project in the Weblate online translation coordination service](https://weblate.org/projects/madewithcc). Weblate
is completely based on free software tools, we invite you to get
familiar with their interface.

## Building the translated Markdown file

`po4a` provides a tool to integrate the translation back into a
Markdown document, `po4a-translate`. I have only made first attempts
with it, and we need to find ways to do several improvements. However,
the following command will give you a good glimpse on how your
finished translation looks:

    po4a-translate -f text -m MadewithCreativeCommonsmostup-to-dateversion.md -p po/es/mwcc.po -l MadewithCreativeCommonsmostup-to-dateversion.es.md -l MadewithCreativeCommonsmostup-to-dateversion.es.md -L utf8 -M utf8 -k 20

What does it all mean?

    -f text

The base format to/from which the translation is done is plain text.

       -m MadewithCreativeCommonsmostup-to-dateversion.md

The name of your source document.

       -p po/es/mwcc.po

Which translation to use. In this case, Spanish translation.

       -l MadewithCreativeCommonsmostup-to-dateversion.es.md

Filename to use for the created translation. Do note that I'm
specifying a name that implies Spanish.

       -L utf8 -M utf8

Which character sets to use, both for the source and destination
files.

       -k 20

Minimum translation percentage for which to produce output. The
default is 80% — To get a glimpse of your work in the early stages of
a translation, use `-k` with a value smaller than the percentage you
have achieved.
